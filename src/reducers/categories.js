const phases = [ 'title', 'countdown', 'question' ];

const initialState = {
  categories: [
    {
      title: 'Top 10 anime crossovers'
    },
    {
      title: 'Одобряем'
    },
    {
      title: 'Дроп зе майк нига бич'
    },
    {
      title: 'Бабулин юбилей'
    },
    {
      title: 'Артём, заебал'
    }
  ]
};

const currentQuestion = (state = initialState, action) => {
  const questions = [100, 200, 300, 400, 500].map((cost, index) => ({cost, id: index}));
  switch(action.type) {
    default:
      return ({
        categories: state.categories.map((cat) => ({...cat, questions})),
      })
  }
};

export default currentQuestion;
