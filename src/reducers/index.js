import { combineReducers, createStore } from 'redux';
import categories from '../reducers/categories';
import currentQuestion from '../reducers/currentQuestion';

const rootReducer = combineReducers({
  categories,
  currentQuestion,
});

export default rootReducer;
